package org.knappi.gachou;

import lombok.extern.slf4j.Slf4j;

import javax.inject.Singleton;

@Singleton
@Slf4j
public class Application {
    public Application() {
        log.info("started");
    }
}
