package org.knappi.gachou;

import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.TestProfile;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.knappi.gachou.testutils.TestDataBuilder;
import org.knappi.gachou.testutils.TestProfileForTestDataSetup;

import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

@QuarkusTest
@TestProfile(TestProfileForTestDataSetup.class)
public class LoginResourceTest {

    @BeforeEach
    public void initialize() {
        TestDataBuilder.startWithWipedData().addUser("testuser", "abc").apply();
    }

    @Test
    public void loginEndpoint_returnsJsonWebToken_forAuth() {

        String token = given()
                .body(Map.of("username", "testuser", "password", "abc"))
                .contentType(ContentType.JSON)
                .when()
                .post("/api/login")
                .body()
                .jsonPath()
                .getString("token");

        given()
                .header("Authorization", "Bearer " + token)
                .when()
                .get("/api/users/me")
                .then()
                .assertThat()
                .statusCode(200)
                .body("username", is("testuser"));
    }

    @Test
    public void loginEndpoint_answers401error_ifPasswordWrong() {

        given()
                .body(Map.of("username", "testuser", "password", "wrong-password"))
                .contentType(ContentType.JSON)
                .when()
                .post("/api/login")
                .then()
                .assertThat()
                .statusCode(401)
                .body("status", is(401))
                .body("message", equalTo("Wrong username or password"));
    }
}
