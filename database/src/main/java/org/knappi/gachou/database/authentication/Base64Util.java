package org.knappi.gachou.database.authentication;

import java.util.Base64;

class Base64Util {
    public static String encodeBase64(byte[] rawData) {
        return Base64.getEncoder().encodeToString(rawData);
    }

    public static byte[] decodeBase64(String base64) {
        return Base64.getDecoder().decode(base64);
    }
}
