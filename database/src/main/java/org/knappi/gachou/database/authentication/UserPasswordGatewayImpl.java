package org.knappi.gachou.database.authentication;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.knappi.gachou.core.usecases.authentication.gateway.UserAccounts;
import org.knappi.gachou.database.authentication.entity.UserPasswordEntity;
import org.wildfly.security.password.Password;
import org.wildfly.security.password.PasswordFactory;
import org.wildfly.security.password.interfaces.BCryptPassword;
import org.wildfly.security.password.spec.EncryptablePasswordSpec;
import org.wildfly.security.password.spec.IteratedSaltedPasswordAlgorithmSpec;

import javax.inject.Singleton;
import javax.transaction.Transactional;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;
import java.util.Optional;

import static org.knappi.gachou.database.authentication.Base64Util.decodeBase64;

@RequiredArgsConstructor
@Singleton
@Slf4j
public class UserPasswordGatewayImpl implements UserAccounts {

    public static final int ITERATIONS = 10;

    @Override
    @Transactional
    public void addUser(String username, char[] password) {
        new UserSaveOperation(username, password).addUser();
    }

    @Override
    @Transactional
    public void upsertUser(String username, char[] password) {
        new UserSaveOperation(username, password).upsertUser();
    }


    @Override
    public boolean verifyPassword(String username, char[] guess) {
        Optional<UserPasswordEntity> optionalUserEntity =
                UserPasswordEntity.findByUsername(username);
        if (optionalUserEntity.isEmpty()) {
            log.error("User '{}' not found", username);
            return false;
        }
        UserPasswordEntity userPasswordEntity = optionalUserEntity.get();

        if (verify(userPasswordEntity, guess)) {
            return true;
        }
        log.error("Invalid password for user '{}'", username);
        return false;

    }

    private static boolean verify(UserPasswordEntity userPasswordEntity, char[] guess) {
        byte[] salt = decodeBase64(userPasswordEntity.getSalt());
        int iterationCount = userPasswordEntity.getIterationCount();
        byte[] hashFromGuess = createBcryptHash(salt, iterationCount, guess);
        byte[] hashFromEntity = decodeBase64(userPasswordEntity.getHash());
        return Arrays.equals(hashFromEntity, hashFromGuess);
    }

    static byte[] createBcryptHash(byte[] salt, int iterations, char[] password) {
        try {
            return tryCreateBcryptHash(salt, iterations, password);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("Configuration error: Configured algorithm not found", e);
        } catch (InvalidKeySpecException e) {
            throw new RuntimeException("Configuration error: Invalid key spec", e);
        }
    }

    private static byte[] tryCreateBcryptHash(byte[] salt, int iterations, char[] password)
            throws NoSuchAlgorithmException, InvalidKeySpecException {
        Password result = generatedPassword(salt, iterations, password);
        return result.castAs(BCryptPassword.class).getHash();
    }

    private static Password generatedPassword(byte[] salt, int iterations, char[] password)
            throws NoSuchAlgorithmException, InvalidKeySpecException {
        PasswordFactory instance = PasswordFactory.getInstance(BCryptPassword.ALGORITHM_BCRYPT);
        var algorithmSpec = new IteratedSaltedPasswordAlgorithmSpec(iterations, salt);
        var passwordSpec = new EncryptablePasswordSpec(password, algorithmSpec);
        return instance.generatePassword(passwordSpec);
    }

}
