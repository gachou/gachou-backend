package org.knappi.gachou.database.authentication;

import org.knappi.gachou.core.usecases.authentication.exceptions.UserAlreadyExistsException;
import io.quarkus.test.junit.QuarkusTest;
import lombok.RequiredArgsConstructor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.knappi.gachou.core.usecases.authentication.gateway.UserAccounts;
import org.knappi.gachou.database.authentication.entity.UserPasswordEntity;

import javax.transaction.Transactional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@RequiredArgsConstructor
@QuarkusTest
class UserPasswordGatewayImplTest {

    public static final char[] PASSWORD = "password".toCharArray();
    public static final char[] OTHER_PASSWORD = "other_password".toCharArray();

    private final UserAccounts passwordService;

    @BeforeEach
    @Transactional
    public void cleanup() {
        UserPasswordEntity.deleteAll();
    }


    @Test
    void correctPassword_isVerified() {
        passwordService.addUser("testuser", PASSWORD);
        assertThat(passwordService.verifyPassword("testuser", PASSWORD)).isTrue();
    }

    @Test
    void wrongPassword_isNotVerified() {
        passwordService.addUser("testuser", PASSWORD);
        assertThat(passwordService.verifyPassword("testuser", OTHER_PASSWORD)).isFalse();
    }

    @Test
    void addedUser_cannotBeAddedAgain() {
        passwordService.addUser("testuser", PASSWORD);
        assertThatThrownBy(() -> passwordService.addUser("testuser",
                PASSWORD
        )).isInstanceOf(UserAlreadyExistsException.class);
    }

    @Test
    void upsertUser_addsNewUser() {
        passwordService.upsertUser("testuser", PASSWORD);
        assertThat(passwordService.verifyPassword("testuser", PASSWORD)).isTrue();
    }

    @Test
    void upsertUser_updatesPasswordOfExistingUser() {
        passwordService.addUser("testuser", PASSWORD);
        passwordService.upsertUser("testuser", OTHER_PASSWORD);
        assertThat(passwordService.verifyPassword("testuser", PASSWORD)).isFalse();
        assertThat(passwordService.verifyPassword("testuser", OTHER_PASSWORD)).isTrue();
    }
}