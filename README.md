# gachou-backend

## Development 

### Prerequisites

* Install [Quarkus](https://quarkus.io/get-started/)
* Clone the repository

## Dev-Server and Tests

Run 

```bash
docker-compose up -d 
quarkus dev
```
in the project directory

## Build and run docker-image

This builds a docker-image containing the JAR build.
Due to the build-duration of a native-image and the limited
number of minutes on `gitlab.com` shared CI runners, we don't
build native images by default.

```bash
./mvnw install
docker-compose --profile backend build
docker-compose --profile backend up
```

## Build native image

```
docker-compose up -d 
./mvnw install -Pnative
```

# Architecture Rules

## Common rules

* Prefer constructor injection via `@RequiredArgsConstructor` and `final` fields  
  over `@Inject`, since it allows immutable classes and does not depend
  on CDI annotations.
* Endpoint-resources are an exception to this rule. They don't work
  properly with constructor injection. Use `@Inject` there
* Use `@NotNullByDefault` where possible. Data transfer objects
  need to user jax-rs `@NotNull` to be represented properly in the 
  OpenAPI-spec.

## `core` module

There are two packages: `usecases` and `domain`.

* `domain` only contains core domain models (i.e. 
entities in clean-architecture speak).
* `usecases` contains business-logic (use-cases).
* `utils` contains generic utilities that are not tied to a use-case.

Each use-case can contain sub-packages.

* `initialization` contains classes with StartupObservers etc, that 
  are executed on startup.
* `gateway` contains interfaces that are implemented in other modules
* `model` contains specific model classes for this use-case and the 'gateway',  
   like parameter structures for services, and return types.
* `exceptions` contains exceptions that are specific to this use-case.

Only the methods of `*Service` classes in the use-case package itself should be called 
from outside the use-case. It is OK to have functions in services, that have 
only a single line, calling a gateway function.

## `rest-api` module
 
Each endpoint is in its own package below `org.knappi.gachou.rest_api.endpoints`.
Each endpoint package has the following structure:

* The resource class in directly in the endpoint package
* There is a `dto` package containing classes of data-transfer-objects.
  * `*Response` for response body representations
  * `*Request` for request body representations
  * `*Dto` for nested properties within requests or responses
* There is a `mapper` package containing MapStruct mappers that 
  create core model objects from dto and vice versa

