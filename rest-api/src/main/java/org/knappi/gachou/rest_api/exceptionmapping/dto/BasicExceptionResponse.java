package org.knappi.gachou.rest_api.exceptionmapping.dto;

import io.quarkus.runtime.annotations.RegisterForReflection;
import lombok.Builder;
import lombok.Value;

import javax.validation.constraints.NotNull;

@Value
@Builder
@RegisterForReflection
public class BasicExceptionResponse {
    @NotNull
    int status;
    @NotNull
    String message;
}
