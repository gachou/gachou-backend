package org.knappi.gachou.rest_api.endpoints.authentication.dto;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@Builder
public class JwtResponse {
    @NotNull
    private final String token;
}
