package org.knappi.gachou.rest_api.endpoints.users.dto;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class UserResponse {
    String username;
}
