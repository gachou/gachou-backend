package org.knappi.gachou.rest_api.exceptionmapping;

import org.knappi.gachou.rest_api.exceptionmapping.dto.BasicExceptionResponse;

import javax.naming.AuthenticationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class AuthenticationExceptionMapping implements ExceptionMapper<AuthenticationException> {

    @Override
    public Response toResponse(AuthenticationException exception) {
        return Response
                .status(Response.Status.UNAUTHORIZED)
                .entity(BasicExceptionResponse
                        .builder()
                        .status(401)
                        .message(exception.getMessage())
                        .build())
                .build();
    }
}
