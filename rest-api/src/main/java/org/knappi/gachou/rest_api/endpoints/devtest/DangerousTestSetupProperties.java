package org.knappi.gachou.rest_api.endpoints.devtest;

public class DangerousTestSetupProperties {

    public static final String TOKEN = "gachou.dangerous-test-setup-access.token";

    public static final String TOKEN_HEADER_NAME = "X-Gachou-Test-Setup-Token";
}
