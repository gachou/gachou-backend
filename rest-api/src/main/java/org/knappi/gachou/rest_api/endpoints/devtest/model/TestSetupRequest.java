package org.knappi.gachou.rest_api.endpoints.devtest.model;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class TestSetupRequest {
    @NotNull
    private List<TestUserDto> testUsers = List.of();
}
