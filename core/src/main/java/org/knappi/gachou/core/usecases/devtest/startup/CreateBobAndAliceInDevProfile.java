package org.knappi.gachou.core.usecases.devtest.startup;

import io.quarkus.arc.profile.IfBuildProfile;
import io.quarkus.runtime.StartupEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.knappi.gachou.core.usecases.authentication.UserService;

import javax.enterprise.event.Observes;
import javax.inject.Singleton;
import javax.transaction.Transactional;

@RequiredArgsConstructor
@Singleton
@IfBuildProfile("dev")
@Slf4j
public class CreateBobAndAliceInDevProfile {

    private final UserService userService;


    @Transactional
    void onStart(@Observes StartupEvent ev) {
        log.info("Creating dev-users (alice and bob)");
        userService.upsertUser("bob","bob".toCharArray());
        userService.upsertUser("alice","alice".toCharArray());
    }
}
