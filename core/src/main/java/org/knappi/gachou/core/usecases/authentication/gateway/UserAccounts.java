package org.knappi.gachou.core.usecases.authentication.gateway;

public interface UserAccounts {

    void addUser(String username, char[] password);

    void upsertUser(String username, char[] password);

    boolean verifyPassword(String username, char[] password);
}
