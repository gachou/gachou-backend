package org.knappi.gachou.core.usecases.devtest.model;

import lombok.Builder;
import lombok.Singular;
import lombok.Value;

import java.util.List;

@Value
@Builder
public class TestSetup {
    List<TestUser> testUsers;
}
