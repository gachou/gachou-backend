package org.knappi.gachou.core.usecases.devtest.model;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class TestUser {
    String username;
    String password;
}
