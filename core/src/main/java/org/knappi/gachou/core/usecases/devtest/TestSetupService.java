package org.knappi.gachou.core.usecases.devtest;

import lombok.RequiredArgsConstructor;
import org.knappi.gachou.core.usecases.authentication.UserService;
import org.knappi.gachou.core.usecases.authentication.gateway.UserAccounts;
import org.knappi.gachou.core.usecases.devtest.gateway.DangerousDatabaseReset;
import org.knappi.gachou.core.usecases.devtest.model.TestSetup;
import org.knappi.gachou.core.usecases.devtest.model.TestUser;

import javax.inject.Singleton;
import javax.transaction.Transactional;

@RequiredArgsConstructor
@Singleton
public class TestSetupService {
    private final DangerousDatabaseReset databaseReset;
    private final UserService userService;

    @Transactional
    public void setupForTest(TestSetup testSetup) {
        databaseReset.wipeData();
        for (TestUser testUser : testSetup.getTestUsers()) {
            userService.addUser(testUser.getUsername(), testUser.getPassword().toCharArray());
        }
    }
}
