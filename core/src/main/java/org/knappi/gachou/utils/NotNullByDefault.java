package org.knappi.gachou.utils;



import javax.annotation.Nonnull;
import javax.annotation.meta.TypeQualifierDefault;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Nonnull
@TypeQualifierDefault({ElementType.TYPE,
        ElementType.PARAMETER,
        ElementType.METHOD,
        ElementType.LOCAL_VARIABLE,
        ElementType.FIELD,
        ElementType.PACKAGE})
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.PACKAGE, ElementType.TYPE})
public @interface NotNullByDefault {
}